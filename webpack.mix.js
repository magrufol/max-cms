const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const admin_res_path = 'resources/assets/admin/';
const front_res_path = 'resources/assets/frontend/';

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/admin-app.scss', 'public/css/admin');

mix.styles([
    admin_res_path + 'bootstrap/css/bootstrap.min.css',
    admin_res_path + 'font-awesome/4.5.0/css/font-awesome.min.css',
    admin_res_path + 'ionicons/2.0.1/css/ionicons.min.css',
    admin_res_path + 'plugins/icheck/minimal/_all.css',
    admin_res_path + 'plugins/datepicker/datepicker3.css',
    admin_res_path + 'plugins/select2/select2.min.css',
    admin_res_path + 'plugins/datatables/datatables.bootstrap.css',
    admin_res_path + 'dist/css/AdminLTE.min.css',
    admin_res_path + 'dist/css/skins/_all-skins.min.css',
], 'public/css/admin/admin.css');

mix.scripts([
    admin_res_path + 'plugins/jQuery/jquery-2.2.3.min.js',
    admin_res_path + 'bootstrap/js/bootstrap.min.js',
    admin_res_path + 'plugins/select2/select2.full.min.js',
    admin_res_path + 'plugins/datepicker/bootstrap-datepicker.js',
    admin_res_path + 'plugins/datatables/jquery.dataTables.min.js',
    admin_res_path + 'plugins/datatables/dataTables.bootstrap.min.js',
    admin_res_path + 'plugins/slimScroll/jquery.slimscroll.min.js',
    admin_res_path + 'plugins/fastclick/fastclick.js"',
    admin_res_path + 'dist/js/app.min.js',
    admin_res_path + 'dist/js/demo.js',
    admin_res_path + 'dist/js/scripts.js',
], 'public/js/admin/admin.js');

mix.copy(admin_res_path + 'bootstrap/fonts', 'public/fonts');
mix.copy(admin_res_path + 'dist/fonts', 'public/fonts');
mix.copy(admin_res_path + 'dist/img', 'public/img');


mix.styles([
    front_res_path + 'css/bootstrap.min.css',
    front_res_path + 'css/font-awesome.min.css',
    front_res_path + 'css/animate.min.css',
    front_res_path + 'css/owl.carousel.css',
    front_res_path + 'css/owl.theme.css',
    front_res_path + 'css/owl.transitions.css',
    front_res_path + 'css/style.css',
    front_res_path + 'css/responsive.css',
], 'public/css/front.css');

mix.scripts([
    front_res_path + 'js/jquery-1.11.3.min.js',
    front_res_path + 'js/bootstrap.min.js',
    front_res_path + 'js/owl.carousel.min.js',
    front_res_path + 'js/jquery.stickit.min.js',
    front_res_path + 'js/menu.js',
    front_res_path + 'js/scripts.js',
],'public/js/front.js');

mix.copy(front_res_path + 'fonts', 'public/fonts');
mix.copy(front_res_path + 'images', 'public/images');
